var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
var howToMeasureDA = require('./howToMeasureDA');


// exports.uploadImageHowToMeasure = function (req, res) {
//     try {
//       const params = {
//         Bucket: env.ProductBucket + '/' + 'images' + '/' + 'howtomeasure' + '/' + req.params.id, // create a folder and save the image
//         Key: req.file.originalname,
//         Body: req.file.buffer,
//       };
//       s3.upload(params, function (err, data) {
//         if (err) {
//           console.log(err);
//         } else {
//            res.status(200).json(params);
//         }
//       });
//     } catch (error) {
//       console.log(error);
//     }
//     } 

exports.uploadImageHowToMeasure = function (req, res) {
  try {


    howToMeasureDA.uploadImageHowToMeasure(req, res);


  } catch (error) {
    console.log(error);
  }
}
