var measurementDA = require('./measurementImageDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');


exports.uploadMeasurementImage = function (req, res) {
    try {
      measurementDA.uploadMeasurementImage(req, res);
    } catch (error) {
      console.log(error);
    }
    } 
    exports.getObjectBuffer = function (req, res) {
      try {
        measurementDA.getObjectBuffer(req, res);
      } catch (error) {
        console.log(error);
      }
      } 
      exports.downloadDropBoxImage = function (req, res) {
        try {
          measurementDA.downloadDropBoxImage(req, res);
        } catch (error) {
          console.log(error);
        }
        } 
