var productLandingDA = require('./productLandingDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');


exports.uploadLandingImageOne = function (req, res) {
    try {
        productLandingDA.uploadLandingImageOne(req, res);
    } catch (error) {
      console.log(error);
    }
} 
exports.uploadLandingImageTwo = function (req, res) {
    try {
        productLandingDA.uploadLandingImageTwo(req, res);
    } catch (error) {
      console.log(error);
    }
} 

exports.uploadContentOneImage = function (req, res) {
    try {
        productLandingDA.uploadContentOneImage(req, res);
    } catch (error) {
      console.log(error);
    }
} 
exports.uploadContentTwoImage = function (req, res) {
    try {
        productLandingDA.uploadContentTwoImage(req, res);
    } catch (error) {
      console.log(error);
    }
} 

