var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
var request = require('request');
var https = require('https');
var fs = require('fs');

exports.uploadLandingImageOne = function (req, res) {
    const base64Data = Buffer.from(req.body.landingPageImageOne.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.landingPageImageOne.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.ProductBucket + '/' + 'images' + '/' + 'productLandingImage' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.bannerImageName,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }

  exports.uploadLandingImageTwo = function (req, res) {
    const base64Data = Buffer.from(req.body.landingPageImageTwo.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.landingPageImageTwo.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.ProductBucket + '/' + 'images' + '/' + 'productLandingImage' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.stripBannerImageName,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }


  exports.uploadContentOneImage = function (req, res) {
    const base64Data = Buffer.from(req.body.landingPageImageThree.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.landingPageImageThree.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.ProductBucket + '/' + 'images' + '/' + 'productLandingImage' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.contentImageNameOne,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }

  exports.uploadContentTwoImage = function (req, res) {
    const base64Data = Buffer.from(req.body.landingPageImageFour.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.landingPageImageFour.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.ProductBucket + '/' + 'images' + '/' + 'productLandingImage' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.contentImageNameTwo,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }
