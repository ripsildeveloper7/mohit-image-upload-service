var vendorMgr = require('./vendor/vendorMgr');
var upload = require('../config/multer.config');

module.exports = function (app) {
    app.route('/createcancelchequeimage/:id')                 // ADD  Cancel Cheque
    .put(upload.single('single'), vendorMgr.uploadImageChancelCheque);

/*     app.route('/updatecancelchequeimage/:id')                 // Update  Cancel Cheque
    .put(upload.single('single'), vendorMgr.updateImageCancelCheque); */

    app.route('/createdigitalsignatureimage/:id')                 // ADD  Digital Signature
    .put(upload.single('single'), vendorMgr.uploadImageDigitalSignature);

    /* app.route('/updatedigitalsignatureimage/:id')                 // Update  Digital Signature
    .put(upload.single('single'), vendorMgr.updateImageDigitalSignature); */
}
