/* var vendorDetails = require('../../model/vendorAccount.model');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

exports.uploadImageCancelCheque = function (req, file, res) {
    vendorDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            data.cancelledCheque = file.Key;
            data.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.updateImageCancelCheque = function (req, file, res) {
    vendorDetails.findOne({
        '_id': req.params.id,
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (data.cancelledCheque === file.Key) {
                res.status(200).json(data);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.vendorbucket,
                    Key: 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'cancelcheque' + '/' + data.cancelledCheque
                }, function (err, data) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        vendorDetails.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data1) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                data1.cancelledCheque = file.Key;
                                data1.save(function (err, data) {
                                    if (err) {
                                        res.status(500).json(err);
                                    } else {
                                        data.cancelledCheque = env.VendorImageServerPath + 'vendor' + '/' + data._id + '/' + 'cancelcheque' + '/' + data.cancelledCheque;
                                        res.status(200).json(data);
                                    }
                                })
                            }
                        })
                    }
                })
            }

        }
    });
}

exports.uploadImageDigitalSignature = function (req, file, res) {
    vendorDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            data.digitalSignature = file.Key;
            data.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.updateImageDigitalSignature = function (req, file, res) {
    vendorDetails.findOne({
        '_id': req.params.id,
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (data.digitalSignature === file.Key) {
                res.status(200).json(data);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.vendorbucket,
                    Key: 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'digitalsignature' + '/' + data.digitalSignature
                }, function (err, data) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        vendorDetails.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data1) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                data1.digitalSignature = file.Key;
                                data1.save(function (err, data) {
                                    if (err) {
                                        res.status(500).json(err);
                                    } else {
                                        data.digitalSignature = env.VendorImageServerPath + 'vendor' + '/' + data._id + '/' + 'digitalsignature' + '/' + data.digitalSignature;
                                        res.status(200).json(data);
                                    }
                                })
                            }
                        })
                    }
                })
            }

        }
    });
}
 */