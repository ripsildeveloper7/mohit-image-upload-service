var mongoose = require('mongoose');

const SubCategorySchema = new mongoose.Schema({
    subCategoryName: String,
    subCategoryDescription: String,
    metaTagTitle: String,
    metaTagDescription: String,
    metaTagKeyword: String,
    subCategoryImageName: String
});

module.exports = SubCategorySchema;
