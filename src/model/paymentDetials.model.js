var mongoose = require('mongoose');
const PaymentSchema = new mongoose.Schema({

 currency: String,
 paymentDay: String,
 creditLimit: Number,
 taxesType: String,
 otherPaymentTerm: String,
 formsRequired: String,
 modeOfPayment: String,
 panNo: String,
 tinNo: String,
 cstNo: String,
 gstNo: String,
 bankName: String,
 bankBranch: String,
 bankAddress: String,
 bankAccountNumber: Number,
 micrCode: String,
 neftCode: String,
 rtgsCode: String,
 tanNo: String

});
module.exports = PaymentSchema;