var mongoose = require('mongoose');
const signatureSchema = new mongoose.Schema({

    vendorAuth: String,
    vendDate: Date,
    companyAuth: String,
    companyDate: Date,
    regBy: String,
    regDate: Date

});
module.exports = signatureSchema;