var mongoose = require('mongoose');
const officeAddressFormSchema = new mongoose.Schema({
    officeAddressLine1: String,
    officeAddressLine2: String,
    officeCity: String,
    officeState: String,
    officePincode: String
});
module.exports = officeAddressFormSchema;