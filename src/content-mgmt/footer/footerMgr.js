var s3 = require('../../config/s3.config');
var footerDA = require('./footerDA');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

// exports.uploadFooterImageMulter = function (req, res) {
//   try {
//     const params = {
//       Bucket: env.homebucket + '/' + 'images' + '/' + 'footer' + '/' + req.params.id, // create a folder and save the image
//       Key: req.file.originalname,
//       Body: req.file.buffer,
//     };

//     s3.upload(params, function (err, data) {
//       if (err) {
//         console.log(err);
//       } else {
//         /* headerDA.uploadImageMulter(req, params, res); */
//         res.status(200).json(params);
//       }
//     });
//   } catch (error) {
//     console.log(error);
//   }
//   } 
exports.uploadFooterBaseSingleImage = function(req, res) {
  try {
    footerDA.uploadFooterBaseSingleImage(req, res);
  } catch (error) {
    console.log(error);
  }
}