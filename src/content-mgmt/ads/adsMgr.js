var adsDA = require('./adsDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

exports.uploadImageADS = function (req, res) {
  try {
    const params = {
      Bucket: env.homebucket + '/' + 'images' + '/' + 'smallbanner' + '/' + req.params.id, // create a folder and save the image
      Key: req.file.originalname,
      Body: req.file.buffer,
    };
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        /*  adsDA.uploadImageADS(req, params, res); */
        res.status(200).json(params);
      }
    });
  } catch (error) {
    console.log(error);
  }
}

exports.updateADSImage = function (req, res) {
  try {
    const params = {
      Bucket: env.homebucket + '/' + 'images' + '/' + 'smallbanner' + '/' + req.params.id, // create a folder and save the image
      Key: req.file.originalname,
      Body: req.file.buffer,
    };
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        /* adsDA.updateADSImage(req, params, res); */
        res.status(200).json(params);
      }
    });
  } catch (error) {
    console.log(error);
  }
}

exports.uploadAdsBaseSingleImage = function (req, res) {
  try {


    adsDA.uploadAdsBaseSingleImage(req, res);


  } catch (error) {
    console.log(error);
  }
}
